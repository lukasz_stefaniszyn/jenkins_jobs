package com.example.pipeline;

class BaseWorker implements Serializable {
	
	/**
	 * Reference to Jenkins script
	 */
	def protected script
	
	BaseWorker(script) {
		this.script = script;
	}
}