package com.example.pipeline


import com.example.pipeline.BaseWorker


class Hello extends BaseWorker{

	
	public Hello(script) {
		super(script)
	}

	/**
	 * @return
	 */
	public void hi(String name) {
		script.echo("Hello ${name}. My job url is: " + script.env.JOB_URL)
		}
}


