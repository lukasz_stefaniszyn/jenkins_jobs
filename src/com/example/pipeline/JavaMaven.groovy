package com.example.pipeline

import com.cloudbees.groovy.cps.NonCPS
import com.example.pipeline.BaseWorker


class JavaMaven extends BaseWorker{

	def private static final dockerImageName = "lucst/devonfwe2e:v2-0.4"
	
	public JavaMaven(script) {
		super(script)
	}


	public void compile() {
		script.echo("compile")
		script.sh("ls")
		
		def image = script.docker.image(dockerImageName);
		image.inside(""){ c ->
            script.sh"""
					echo mvn clean install -DskipTests=true
					echo mvn compile
					"""
		}
		
	}

	@NonCPS
	public void test() {
		script.echo("test")
		def image = script.docker.image(dockerImageName);
		image.inside(""){ c ->
            script.sh"""
					echo mvn test
					"""
		}
	}
	
}


