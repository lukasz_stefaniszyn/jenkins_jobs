#!groovy

import com.example.pipeline.Hello
import com.example.pipeline.JavaMaven


/* ------------------------------- Hello ------------------------------- */

def hello() {
  return new Hello(this)
}

/* ------------------------------- Utils ------------------------------- */

def javaMaven() {
  return new JavaMaven(this)
}

/* ------------------------------ Veracode ----------------------------- */



return this
